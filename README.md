Youtube discovery project.

I have some youtube channels that i like and because i use a client with offline subscriptions there is a
discovery-element missing for me. To solve a part of it i made this script which performs depth-first search starting
from a stack of channels to find what these channels are subscribed to or what they like. These channels are then
consequently added to the top of the stack until a certain depth is reached.

At the end of the search we'll translate youtube channel or user IDs to channel names, for ease of reference.

The most annoying part is that the youtube homepage changes constantly, and that the scraping might break. Youtube makes
it actively difficult to have selenium click the `accept cookies' button, among other things.

There is the option to dump the results to a DOT file for drawing the graph out nicely.

Be careful with using large stack depths, the growth of the search space is exponential.

# Translations
The `resources/translations.csv` file contains a mapping of channel ids to channel names. The channel names are taken
from the html `title` tag when the page was visited by selenium, we simply save the names so that we don't have to visit
them again just to translate the id to a name. This speeds up the execution between runs quite a bit.

An example of a translation entry is: `"channel/UC1_uAIS3r8Vu6JjXWvastJg","Mathologer"`

# Stack
The `resources/stack.csv` file contains the initial stack in csv format. The first column denotes the initial depth of
the item (usually 0, but can be set higher if you don't want to search as deep for this channel as for the others), and
the second column denotes the channel id to visit.

An example of an initial stack entry is: ` 0,"channel/UCTGHqw41qk_WyK3wJK7nweg"`
