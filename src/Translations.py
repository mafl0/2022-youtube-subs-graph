import re
import os
import csv

# Class to handle translations and translation saving

class Translations(object):

    _translations = {}
    _fileName     = ""
    _h            = None
    _synchronise  = False

    def __init__(self, fileName, synchronise = False):

        self._translations = {}
        self._fileName     = fileName
        self._synchronise  = synchronise

        self._h = open(self._fileName, "a+")

        if os.path.exists(fileName):
            self._translations = Translations.parse(fileName)

    def __del__(self):
        self._h.close()

    def add(self, url, name):
        if url not in self._translations.keys():
            self._translations[url] = name

            # If we need to keep the translation file synchronised, write it to
            # disk.
            if (self._synchronise):
                self.saveSingle(url, name)

    def exists(self, url):
        return url in self._translations

    # Save a single translation to the translation file. Stateful. It expects
    # the file handle self._h to be valid. It will always write the entry out
    # even if the output file already contains this entry. It will add the entry
    # to memory if the key does noet exist yet.
    
    def saveSingle(self, url, name):
        if url not in self._translations.keys():
            self.add(url, name)

        self._h.write(Translations._formatAsCSV(url, name))

    @staticmethod
    def channelNameFromTitle(channelTitle):
        return re.sub(" - YouTube", "", channelTitle)
    
    # CSV format that we expect. Simply so that we have the format in one place.

    @staticmethod
    def _formatAsCSV(url, name):
        return f"\"{url}\",\"{name}\"\n"

    # Parse a csv file containing translations. The format of the translations
    # is "url","translation". All other fields are ignored.

    @staticmethod
    def parse(file):
        translations = {}

        with open(file, "r") as h:
            lines = csv.reader(h, delimiter=',', quotechar='"')
            for line in lines:  
                if len(line) >= 2:
                    url  = line[0]
                    name = line[1]
                    translations[url] = name

        return translations

    # Save an entire translation table at once to a specified file. If the file
    # exists, the contents are appended. No further checking is done, duplicate
    # entries are not removed.

    @staticmethod
    def save(file, translations):
        with open(file, "a+") as h:
            for url in translations.keys():
                name = translations[url]
                h.write(Translations._formatAsCSV(url, name))
