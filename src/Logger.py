import sys
import time

# Loglevels

DEBUG    = 4
INFO     = 3
WARNING  = 2
ERROR    = 1
CRITICAL = 0

# Simple logging class

class Logger(object):

    _verbosity = CRITICAL
    _logDates  = True
    _file      = None

    def __init__(self, verbosity, file = sys.stderr, logDates = True):
        self.setVerbosity(verbosity)

        self._file     = file
        self._logDates = logDates

    def setVerbosity(self, verbosity):
        self._verbosity = verbosity

    def line(self, msg, verbosity):
        msgString = str(msg)
        date      = time.strftime("%Y-%m-%d %T")

        outStr    = ""

        if verbosity <= self._verbosity:
            if self._logDates:
                outStr = f"[{date}]: {msgString}"
            else:
                outStr = f"{msgString}"

            print(outStr, file = self._file)
