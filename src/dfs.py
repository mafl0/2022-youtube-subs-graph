#!/usr/bin/env python

import sys
import re
import time

import networkx

from selenium                           import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions         import NoSuchElementException
from selenium.webdriver.common.by       import By

from Translations import Translations
from Stack        import Stack
from CookieGate   import CookieGate
from Graph        import Graph
from Logger       import *

YT_BASE_URL = "https://www.youtube.com"

_translations = Translations("../resources/translations.csv", synchronise = True)
_initialStack = Stack.parse("../resources/stack.csv")
_logger       = Logger(DEBUG)

# Click the arrow until it disappears, then all channels are loaded. If there is
# no arrow, just ignore this.
def loadAllChannels(driver):

    _logger.line("Loading all channels...", DEBUG)

    try:
        time.sleep(2)

        arrows = driver.find_elements(By.CSS_SELECTOR, "div#right-arrow ytd-button-renderer")

        for arrow in arrows:
            while arrow.is_displayed():
                arrow.click()
                _logger.line("Found right-arrow to click.", DEBUG)
    except NoSuchElementException:
        _logger.line("Could not find right-arrow to click. Ignoring.", DEBUG)

def scrapeConnectedNodes(driver, channel, depth):

    _logger.line(f"Getting connected nodes for {channel}", DEBUG)

    ret = []

    # Open the channel page, wait a bit.
    driver.get(f"{YT_BASE_URL}/{channel}")
    time.sleep(1) # wait until `channels' button is visible TODO

    # Add a translation entry for this channel
    _translations.add(channel, Translations.channelNameFromTitle(driver.title))

    # Locate and click the `channels' tab by case insensitive, linear search
    for button in driver.find_element(By.ID, "tabsContent").find_elements(By.CLASS_NAME, "tab-content"):
        if re.match("channels", button.text, re.I):
            button.click()
    time.sleep(1) # wait until arrow button is visible TODO

    # Load all channels by clicking the arrow until it disappears
    loadAllChannels(driver)

    # Iterate through all anchors within a scroll container, saving the ones
    # that have a specific id.
    nodes = driver.find_elements(By.TAG_NAME, "ytd-section-list-renderer")[0].find_elements(By.TAG_NAME, "a")
    for node in nodes:
        if node.get_attribute("id") == "channel-info":
            channelURL = re.sub(f"{YT_BASE_URL}/", "", node.get_attribute("href"))
            ret.append((depth + 1, channelURL))

    return ret

# up to a certain depth starting with a given stack.
def dfs(driver, stack, maxDepth):

    _logger.line("Starting traversal, initial stack:", DEBUG)
    _logger.line(str(stack), DEBUG)

    # Visited channels, do not visit them again
    graph = networkx.DiGraph()

    channelsEncountered = 0
    channelsVisited     = 0

    while not stack.empty():
        (depth, channel) = stack.pop()

        _logger.line("Stack size: " + str(stack.length()), DEBUG)
        _logger.line(f"Current depth: {depth}/{maxDepth}", DEBUG)
        _logger.line(f"Total channels encountered: {channelsEncountered}", DEBUG)
        _logger.line(f"Total channels visited: {channelsVisited}", DEBUG)

        if channel not in graph.nodes() and depth < maxDepth:
            graph.add_node(channel)

            for node in scrapeConnectedNodes(driver, channel, depth):
                stack.push(node)
                graph.add_edge(channel, node[1])

            channelsVisited += 1

        channelsEncountered += 1

    channelsUntranslated = channelsEncountered - channelsVisited
    _logger.line("Finished traversal", DEBUG)
    _logger.line(f"Directly translatable channels: {channelsVisited}", DEBUG)
    _logger.line(f"Channels left to translate by visit: {channelsUntranslated}", DEBUG)

    return graph 

# Given a graph consisting of uri's, translate it to a graph consisting of the
# names of channels. It's a bit convoluted but it works.
def relabelGraph(driver, graph):
    
    _logger.line("Starting renaming of the graph from URI to name representation", DEBUG)

    # Complete the translations mapping
    for uri in graph.nodes():
        if not _translations.exists(uri):
            driver.get(f"{YT_BASE_URL}/{uri}")
            name = Translations.channelNameFromTitle(driver.title)
            _logger.line(f"Added translation for {uri} -> {name}", DEBUG)
            _translations.add(uri, name)

    networkx.relabel_nodes(graph, _translations._translations, copy=False)

if __name__ == "__main__":

    maxDepth = 4

    stack = Stack(maxDepth, _initialStack)

    options = Options()
    options.add_argument("--headless") # Headless mode for speed and less distraction, turn off for debugging

    driver = webdriver.Firefox(options = options)
    driver.set_window_size(1920, 1080)
    driver.get(YT_BASE_URL)

    CookieGate.solve(driver)

    graph = dfs(driver, stack, maxDepth)
    relabelGraph(driver, graph)

    for degree, channelName in Graph.in_degree(graph):
        print(f"{degree}\t{channelName}")

    driver.quit()
