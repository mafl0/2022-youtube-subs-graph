import csv

# Stack class which gives us a bit more utility around the standard python list
# append and pop functions.

class Stack(object):

    _maxDepth = 0
    _stack    = []
    _fileName = ""

    def __init__(self, maxDepth, initialStack = []):
        self._maxDepth = maxDepth
        self._stack    = initialStack

    def __str__(self):
        n     = self.length()
        return f"Stack with {n} elements:\n" + "".join([f"({depth},{url})\n" for (depth, url) in self._stack])

    def push(self, node):

        (depth, url) = node

        if (depth < self._maxDepth):
            self._stack.append((depth, url))
    
    def pop(self):
        return self._stack.pop()

    def length(self):
        return len(self._stack)

    def empty(self):
        return self.length() == 0

    @staticmethod
    def parse(file):

        stack = []

        with open(file, "r") as h:
            lines = csv.reader(h, delimiter=',', quotechar='"')
            for line in lines:

                # Skip comments, poor mans version
                if len(line) > 0 and len(line[0]) > 0 and line[0][0] == '#':
                    continue

                initialDepth = int(line[0])
                url          = str(line[1])
                stack.append((initialDepth, url))
        
        return stack

    @staticmethod
    def save(file, stack):
        with open(file, "a+") as h:
            for (depth, url) in stack:
                h.write(f"{depth},\"{url}\"\n")
