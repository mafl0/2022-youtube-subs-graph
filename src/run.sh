#!/usr/bin/env bash

# Runner script for the python program. Simply cleans up crashed or incomplete
# marionette firefox sessions and their tempdata so that our ram doesnt fill up
# over time

# Clean up session storage
rm -rf /tmp/rust_mozprofile*

# Kill all marionette firefox processes
for p in $(pgrep firefox); do
  if grep marionette /proc/$p/cmdline >> /dev/null; then
    echo "$p is a firefox marionette. killing"
    kill $p
  fi
done

# run the script
python ./dfs.py
