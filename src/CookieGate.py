import time
import re

from selenium                           import webdriver
from selenium.common.exceptions         import NoSuchElementException
from selenium.webdriver.common.by       import By
from selenium.webdriver.common.action_chains import ActionChains

# Click accept on the youtube cookie gate. Youtube does active counter-botting
# so they actually make it reasonably difficult to click the `accept button'. We
# just brute force it by looking through all spans containing the `accept all'
# string and clicking on the position. 
#
# NOTE: it is important that the browser window is large enough, otherwise we
# might not be able to click.

class CookieGate(object):

    @staticmethod
    def solve(driver):
        time.sleep(1)

        strategies = [ lambda: CookieGate._strategy_1(driver)
                     ]

        for strategy in strategies:
            strategy()

    @staticmethod
    def _strategy_1(driver):

        spans = driver.find_elements(By.TAG_NAME, "span")
        spans = list(filter(lambda x: re.match("accept all", x.text, re.I), spans))

        for span in spans:
            if re.match("accept all", span.text, re.I):
                ActionChains(driver).move_to_element(span).click().perform()
                break
