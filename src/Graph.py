import networkx

class Graph(object):
  
    # Count the in-degree of a graph. This is the interesting measure for us
    # because we measure popularity of a channel by how often it was subscribed
    # to by others.
    @staticmethod
    def _count_in_degree(graph, node):
        degree = 0
        for u, v in graph.edges():
            if v == node:
                degree += 1

        return degree

    # Return a list of the in-degree of all nodes in a graph.
    @staticmethod
    def in_degree(graph):
        return sorted([(Graph._count_in_degree(graph, node), node) for node in graph.nodes()])
